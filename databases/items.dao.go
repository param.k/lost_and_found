package databases

import (
	"fmt"
	"time"

	"gitlab.com/zolo/lost_and_found/domain/items"
	"gorm.io/gorm"
)

type ItemDao struct {
	db *gorm.DB
}

func NewItemDaoConfig(db *gorm.DB) items.ItemDaoConfig {
	return &ItemDao{
		db: db,
	}
}

//FIXME: pagination using offsets is not optimal

func (dao *ItemDao) GetAllItemsByStartDate(date time.Time, page int64, isLost bool, sort string) ([]*items.Item, error) {
	offset := int(10 * (page - 1))
	var foundItems []*items.Item
	sortStrategy := fmt.Sprintf("date %v", sort)
	results := dao.db.
		Limit(10).
		Offset(offset).
		Order(sortStrategy).
		Where("is_lost = ? AND date >= ?", isLost, date).
		Find(&foundItems)
	if results.Error != nil {
		return nil, results.Error
	}
	return foundItems, results.Error
}
func (dao *ItemDao) GetAllItemsByLocation(location int64, page int64, isLost bool, sort string) ([]*items.Item, error) {
	offset := int(10 * (page - 1))
	var foundItems []*items.Item
	sortStrategy := fmt.Sprintf("date %v", sort)
	results := dao.db.
		Limit(10).
		Offset(offset).
		Order(sortStrategy).
		Where("is_lost = ? AND location = ?", isLost, location).
		Find(&foundItems)
	if results.Error != nil {
		return nil, results.Error
	}
	return foundItems, results.Error
}

func (dao *ItemDao) GetAllItemsByStartDateAndLocation(date time.Time, location int64, page int64, isLost bool, sort string) ([]*items.Item, error) {
	offset := int(10 * (page - 1))
	var foundItems []*items.Item
	sortStrategy := fmt.Sprintf("date %v", sort)
	results := dao.db.
		Limit(10).
		Offset(offset).
		Order(sortStrategy).
		Where("is_lost = ? AND location = ? AND date > ?", isLost, location, date).
		Find(&foundItems)
	if results.Error != nil {
		return nil, results.Error
	}
	return foundItems, results.Error
}
func (dao *ItemDao) PostItem(item *items.Item) error {
	result := dao.db.Create(&item)
	if result.Error != nil {
		return result.Error
	}
	return nil

}
func (dao *ItemDao) GetAllItems(page int64, isLost bool, sort string) ([]*items.Item, error) {
	offset := int(10 * (page - 1))
	sortStrategy := fmt.Sprintf("date %v", sort)
	var foundItems []*items.Item
	results := dao.db.
		Limit(10).
		Offset(offset).
		Order(sortStrategy).
		Where("is_lost = ?", isLost).
		Find(&foundItems)

	if results.Error != nil {
		return nil, results.Error
	}

	return foundItems, results.Error
}

func (dao *ItemDao) GetItemByID(id int64) (*items.Item, error) {

	var item items.Item
	result := dao.db.First(&item, "id = ?", id)
	if result.Error != nil {
		fmt.Println("Error finding task by ID:", result.Error)
		return nil, result.Error
	}
	return &item, nil
}

func (dao *ItemDao) GetCountOfItems(isLost bool) (int, error) {
	var totalCount int64 = 0
	var item items.Item
	result := dao.db.Model(&item).Where("is_lost=?", isLost).Count(&totalCount)
	if result.Error != nil {
		return 0, result.Error
	}
	return int(totalCount), nil
}

func (dao *ItemDao) AsyncGetCountOfItems(isLost bool, resultChannel chan int, errorChannel chan error) {
	cnt, err := dao.GetCountOfItems(isLost)
	if err != nil {
		errorChannel <- err
	} else {
		resultChannel <- cnt
	}
}
