package items

import (
	"fmt"
	"mime/multipart"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func ContextToDtoMapper(ctx *gin.Context) (*ItemRequestDTO, error) {
	file, _, err := ctx.Request.FormFile("file")

	if err != nil {

		return nil, err
	}
	defer func(file multipart.File) {
		err := file.Close()
		if err != nil {
			fmt.Println("Cannot close file")
		}
	}(file)
	userId, err := strconv.ParseInt(ctx.Request.FormValue("user_id"), 10, 64)
	if err != nil {

		return nil, err
	}
	title := ctx.Request.FormValue("title")
	description := ctx.Request.FormValue("description")
	var isLost bool
	if ctx.Request.FormValue("type") == "lost" {
		isLost = true
	} else {
		isLost = false
	}
	dateString := ctx.Request.FormValue("date")
	date, err := time.Parse("2006-01-02", dateString)
	if err != nil {

		return nil, err
	}
	category, err := strconv.ParseInt(ctx.Request.FormValue("category"), 10, 64)

	if err != nil {

		return nil, err
	}
	var location, collectFrom int64
	if ctx.Request.FormValue("location") != "" {
		location, err = strconv.ParseInt(ctx.Request.FormValue("location"), 10, 64)
		if err != nil {

			return nil, err
		}
	}
	if ctx.Request.FormValue("collect_from") != "" {
		location, err = strconv.ParseInt(ctx.Request.FormValue("collect_from"), 10, 64)
		if err != nil {

			return nil, err
		}
	}
	return &ItemRequestDTO{
		Image_File:  file,
		UserId:      userId,
		Title:       title,
		Description: description,
		IsLost:      isLost,
		Date:        date,
		Category:    category,
		Location:    location,
		CollectFrom: collectFrom,
	}, nil
}
