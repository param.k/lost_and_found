package items

import "time"

// interface
type ItemDaoConfig interface {
	GetAllItemsByStartDate(date time.Time, page int64, isLost bool, sort string) ([]*Item, error)
	GetAllItemsByLocation(location int64, page int64, isLost bool, sort string) ([]*Item, error)
	GetAllItemsByStartDateAndLocation(date time.Time, location int64, page int64, isLost bool, sort string) ([]*Item, error)
	GetAllItems(page int64, isLost bool, sort string) ([]*Item, error)
	AsyncGetCountOfItems(isLost bool, resultChannel chan int, errorChannel chan error)
	GetCountOfItems(isLost bool) (int, error)
	PostItem(*Item) error
	GetItemByID(id int64) (*Item, error)
}
