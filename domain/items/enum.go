package items

//Categories

const (
	STATIONARY  = 1
	ELECTRONICS = 2
	OTHERS      = 100
)

//Location

const (
	PARKING    = 1
	COMMONROOM = 2
	MESS       = 3
	TERRACE    = 4
)

//collectFROM

const (
	WATCHMAN  = 1
	RECEPTION = 2
	ME        = 3
)
