package items

import "math"

func ItemsToResponseDtoMapper(items []*Item, currentPage int, totalResults int) *ItemsResponseDTO {
	maxPage := math.Ceil(float64(totalResults) / 10)
	return &ItemsResponseDTO{
		Items:             items,
		TotalResults:      totalResults,
		CurrentPageNumber: currentPage,
		MaximumPageNumber: int(maxPage),
	}
}
