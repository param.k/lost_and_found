package items

import (
	"time"

	"gorm.io/gorm"
)

type Item struct {
	gorm.Model
	UserId      int64     `json:"user_id" binding:"required"`
	Title       string    `json:"title" binding:"required"`
	Description string    `json:"description" binding:"required"`
	ImageID     string    `json:"img_id" binding:"required"`
	IsLost      bool      `json:"is_lost" binding:"required"`
	IsClaimed   bool      `json:"is_claimed" binding:"required"`
	Date        time.Time `json:"date" binding:"required"`
	Category    int       `json:"category" binding:"required"`
	CollectFrom int       `json:"collect_from"` // found
	Location    int       `json:"location"`     // lost
}
