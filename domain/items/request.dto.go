package items

import (
	"mime/multipart"
	"time"
)

type ItemRequestDTO struct {
	Image_File  multipart.File
	UserId      int64     `json:"user_id" binding:"required"`
	Title       string    `json:"title" binding:"required"`
	Description string    `json:"description" binding:"required"`
	IsLost      bool      `json:"is_lost" binding:"required"`
	Date        time.Time `json:"date" binding:"required"`
	Category    int64     `json:"category" binding:"required"`
	CollectFrom int64     `json:"collect_from"` // found
	Location    int64     `json:"location"`     // lost
}
