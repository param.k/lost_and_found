package items

import (
	"context"

	"github.com/cloudinary/cloudinary-go"
	"github.com/cloudinary/cloudinary-go/api/uploader"
	"github.com/thanhpk/randstr"
)

func DtoToModelMapper(cld *cloudinary.Cloudinary, itemRequestDto *ItemRequestDTO) (*Item, error) {

	var currentContext = context.Background()
	imageID := randstr.Hex(16)

	go cld.Upload.Upload(currentContext, itemRequestDto.Image_File, uploader.UploadParams{PublicID: imageID})

	return &Item{
		Title:       itemRequestDto.Title,
		Description: itemRequestDto.Description,
		ImageID:     imageID,
		UserId:      itemRequestDto.UserId,
		IsLost:      itemRequestDto.IsLost,
		IsClaimed:   false,
		Date:        itemRequestDto.Date,
		Category:    int(itemRequestDto.Category),
		CollectFrom: int(itemRequestDto.CollectFrom),
		Location:    int(itemRequestDto.Location),
	}, nil
}
