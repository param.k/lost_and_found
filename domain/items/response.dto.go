package items

type ItemsResponseDTO struct {
	Items             []*Item `json:"items"`
	TotalResults      int     `json:"total_results"`
	CurrentPageNumber int     `json:"current_page_number"`
	MaximumPageNumber int     `json:"maximum_page_number"`
}
