package items

import (
	"time"

	"github.com/cloudinary/cloudinary-go"
)

//impl

type ItemService struct {
	itemDao ItemDaoConfig
	cld     *cloudinary.Cloudinary
}

func NewItemServiceConfig(itemDao *ItemDaoConfig, cld *cloudinary.Cloudinary) ItemServiceConfig {
	return &ItemService{
		itemDao: *itemDao,
		cld:     cld,
	}
}

// List l=new ArrayList<>()
func (service *ItemService) GetAllLostItems(fromDate time.Time, location int64, page int64, sort string) (*ItemsResponseDTO, error) {
	errorChannel := make(chan error)
	resultChannel := make(chan int)

	go service.itemDao.AsyncGetCountOfItems(true, resultChannel, errorChannel)

	var items []*Item
	var err error
	if fromDate.IsZero() && location == -1 {
		items, err = service.itemDao.GetAllItems(page, true, sort)
	} else if !fromDate.IsZero() && location != -1 {
		items, err = service.itemDao.GetAllItemsByStartDateAndLocation(fromDate, location, page, true, sort)
	} else if fromDate.IsZero() {
		items, err = service.itemDao.GetAllItemsByLocation(location, page, true, sort)
	} else {
		items, err = service.itemDao.GetAllItemsByStartDate(fromDate, page, true, sort)
	}
	if err != nil {

		return nil, err
	}
	var total int
	select {
	case err := <-errorChannel:
		return nil, err

	case total = <-resultChannel:
		break
	}

	return ItemsToResponseDtoMapper(items, int(page), total), nil
}
func (service *ItemService) GetAllFoundItems(fromDate time.Time, location int64, page int64, sort string) (*ItemsResponseDTO, error) {
	errorChannel := make(chan error)
	resultChannel := make(chan int)
	go service.itemDao.AsyncGetCountOfItems(false, resultChannel, errorChannel)
	var items []*Item
	var err error
	if fromDate.IsZero() && location == -1 {
		items, err = service.itemDao.GetAllItems(page, false, sort)
	} else if !fromDate.IsZero() && location != -1 {
		items, err = service.itemDao.GetAllItemsByStartDateAndLocation(fromDate, location, page, false, sort)
	} else if fromDate.IsZero() {
		items, err = service.itemDao.GetAllItemsByLocation(location, page, false, sort)
	} else {
		items, err = service.itemDao.GetAllItemsByStartDate(fromDate, page, false, sort)
	}
	if err != nil {
		return nil, err
	}
	var total int
	select {
	case err := <-errorChannel:
		return nil, err

	case total = <-resultChannel:
		break
	}

	return ItemsToResponseDtoMapper(items, int(page), total), nil
}

func (service *ItemService) PostItem(itemRequestDTO *ItemRequestDTO) (*Item, error) {
	item, err := DtoToModelMapper(service.cld, itemRequestDTO)
	if err != nil {
		return nil, err
	}
	if err := service.itemDao.PostItem(item); err != nil {
		return nil, err
	}
	return item, nil
}

func (service *ItemService) GetItemByID(id int64) (*Item, error) {

	item, err := service.itemDao.GetItemByID(id)
	return item, err
}
