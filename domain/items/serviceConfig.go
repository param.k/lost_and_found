package items

import "time"

// interface
type ItemServiceConfig interface {
	GetAllLostItems(fromDate time.Time, location int64, page int64, sort string) (*ItemsResponseDTO, error)
	GetAllFoundItems(fromDate time.Time, location int64, page int64, sort string) (*ItemsResponseDTO, error)
	PostItem(*ItemRequestDTO) (*Item, error)
	GetItemByID(id int64) (*Item, error)
}
