package initializers

import (
	"log"
	"os"

	"github.com/cloudinary/cloudinary-go"
)

func InitCloudinary() *cloudinary.Cloudinary {
	LoadDotEnv()
	cld, err := cloudinary.NewFromParams(os.Getenv("CLOUDINARY_CLOUD_NAME"), os.Getenv("CLOUDINARY_API_KEY"), os.Getenv("CLOUDINARY_API_SECRET"))
	if err != nil {
		log.Fatal(err)
	}
	return cld
}
