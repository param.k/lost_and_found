package initializers

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/zolo/lost_and_found/domain/items"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitDB() *gorm.DB {
	LoadDotEnv()

	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local", os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("Connection to db successful")
	createItemTable(db)
	return db

}
func createItemTable(db *gorm.DB) {
	if err := db.AutoMigrate(&items.Item{}); err != nil {
		log.Fatal("Cannot create tables")
	} else {
		fmt.Println("Table creation successfull")
	}
}
