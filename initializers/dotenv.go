package initializers

import (
	"github.com/joho/godotenv"
)

func LoadDotEnv() {
	godotenv.Load()

}
