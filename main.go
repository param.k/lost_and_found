package main

import (
	"github.com/cloudinary/cloudinary-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/zolo/lost_and_found/databases"
	"gitlab.com/zolo/lost_and_found/domain/items"
	"gitlab.com/zolo/lost_and_found/initializers"
	"gitlab.com/zolo/lost_and_found/routes"
	"gorm.io/gorm"
)

var (
	db             *gorm.DB
	server         *gin.Engine
	itemDao        items.ItemDaoConfig
	itemService    items.ItemServiceConfig
	itemController routes.ItemController
	cld            cloudinary.Cloudinary
)

func init() {
	db = initializers.InitDB()
	cld = *initializers.InitCloudinary()
	server = gin.Default()
	itemDao = databases.NewItemDaoConfig(db)
	itemService = items.NewItemServiceConfig(&itemDao, &cld)
	itemController = *routes.NewItemController(&itemService)

}
func main() {
	basePath := server.Group("/api/v1")
	itemController.AddItemRoutes(basePath)

	server.Run(":8080")

}
