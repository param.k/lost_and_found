package routes

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/zolo/lost_and_found/domain/items"
	"gorm.io/gorm"
)

type ItemController struct {
	itemService items.ItemServiceConfig
}

func NewItemController(itemService *items.ItemServiceConfig) *ItemController {
	return &ItemController{
		itemService: *itemService,
	}
}

func (controller *ItemController) AddItemRoutes(rg *gin.RouterGroup) {
	itemRoutes := rg.Group("/items")
	itemRoutes.GET("/lost", controller.GetAllLostItems)
	itemRoutes.GET("/found", controller.GetAllFoundItems)
	itemRoutes.POST("", controller.PostItem)
	itemRoutes.GET("/:id", controller.GetItemByID)

}

func (controller *ItemController) PostItem(ctx *gin.Context) {

	itemRequest, err := items.ContextToDtoMapper(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
	}
	item, err := controller.itemService.PostItem(itemRequest)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	ctx.JSON(http.StatusCreated, item)
}
func (controller *ItemController) GetAllLostItems(ctx *gin.Context) {
	queryParams := ctx.Request.URL.Query()
	var location int64 = -1
	if len(queryParams["location"]) > 0 {
		var err error
		location, err = strconv.ParseInt(queryParams["location"][0], 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "Invalid location",
			})
			return
		}
	}
	var page int64 = 1
	if len(queryParams["page"]) > 0 {
		var err error
		page, err = strconv.ParseInt(queryParams["page"][0], 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "Invalid page requested",
			})
			return
		}
	}
	var date time.Time = time.Time{}
	if len(queryParams["from"]) > 0 {
		var err error
		date, err = time.Parse("2006-01-02", queryParams["from"][0])
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "Invalid Date",
			})
			return
		}

	}
	sort := "DESC"
	if len(queryParams["sort"]) > 0 {
		sort = queryParams["sort"][0]
	}
	items, err := controller.itemService.GetAllLostItems(date, location, page, sort)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	if len(items.Items) == 0 {
		ctx.Status(http.StatusNoContent)
		return
	}
	ctx.JSON(http.StatusOK, items)

}
func (controller *ItemController) GetAllFoundItems(ctx *gin.Context) {
	queryParams := ctx.Request.URL.Query()
	var location int64 = -1
	if len(queryParams["location"]) > 0 {
		var err error
		location, err = strconv.ParseInt(queryParams["location"][0], 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "Invalid location",
			})
			return
		}
	}
	var page int64 = 1
	if len(queryParams["page"]) > 0 {
		var err error
		page, err = strconv.ParseInt(queryParams["page"][0], 10, 64)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "Invalid page requested",
			})
			return
		}
	}
	var date time.Time = time.Time{}
	if len(queryParams["from"]) > 0 {
		var err error
		date, err = time.Parse("2006-01-02", queryParams["from"][0])
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "Invalid Date",
			})
			return
		}

	}
	sort := "DESC"
	if len(queryParams["sort"]) > 0 {
		sort = queryParams["sort"][0]
	}
	items, err := controller.itemService.GetAllFoundItems(date, location, page, sort)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	if len(items.Items) == 0 {
		ctx.Status(http.StatusNoContent)
		return
	}
	ctx.JSON(http.StatusOK, items)

}

func (controller *ItemController) GetItemByID(ctx *gin.Context) {

	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Invalid id",
		})
	}
	item, err := controller.itemService.GetItemByID(id)

	if errors.Is(err, gorm.ErrRecordNotFound) {
		ctx.JSON(http.StatusNotFound, gin.H{
			"message": err.Error(),
		})
		return
	} else if err != nil {
		ctx.JSON(http.StatusNotFound, gin.H{
			"message": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, item)

}
